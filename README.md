# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The application is built to help users find airports around them, and view flights and flight details.
* It is built using Xamarin.Android and MVVMCross, logo created using; https://www.graphicsprings.com/logo-maker
* Version : 1.0
* Please note that this project is not yet complete, I am trying to balance work and school, will push updates later in the year and try to finish it.

### How do I get set up? ###

* Visual Studio 2019
* Configuration
* Android API 28 
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Tumelo Mokwena
* ttyanini@gmail.com