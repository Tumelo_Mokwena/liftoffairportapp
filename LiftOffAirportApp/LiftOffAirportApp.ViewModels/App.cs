﻿namespace LiftOffAirportApp.ViewModels
{
    using LiftOffAirportApp.Services.Services;
    using LiftOffAirportApp.Services.Services.Interfaces;
    using LiftOffAirportApp.ViewModels.AirportsMap;
    using MvvmCross;
    using MvvmCross.IoC;
    using MvvmCross.Navigation;
    using MvvmCross.ViewModels;
    using System.Threading.Tasks;

    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();
            RegisterCustomAppStart<CustomAppStart<AirportsViewModel>>();
            Mvx.RegisterSingleton<IAirportsService>(new AirportsService());
        }
    }

    public class CustomAppStart<TViewModel> : MvxAppStart<TViewModel>
        where TViewModel : IMvxViewModel
    {
        public CustomAppStart(IMvxApplication application, IMvxNavigationService navigationService) : base(application, navigationService)
        {
        }

        protected async override Task NavigateToFirstViewModel(object hint)
        {
            await NavigationService.Navigate<TViewModel>();
        }
    }
}
