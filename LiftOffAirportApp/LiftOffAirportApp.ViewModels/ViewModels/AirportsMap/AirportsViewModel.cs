﻿namespace LiftOffAirportApp.ViewModels.AirportsMap
{
    using LiftOffAirportApp.Models.Models;
    using LiftOffAirportApp.Services.Services.Interfaces;
    using MvvmCross.Navigation;
    using MvvmCross.ViewModels;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class AirportsViewModel : MvxViewModel
    {
        public IEnumerable<AirportModel> Airports
        {
            get => _airports;
            set => SetProperty(ref _airports, value);
        }
        private IEnumerable<AirportModel> _airports;

        private IMvxNavigationService _navigationService;
        private IAirportsService _airportsService;

        public AirportsViewModel(IMvxNavigationService navigationService, IAirportsService airportsService)
        {
            _navigationService = navigationService;
            _airportsService = airportsService;
        }

        public async Task GetAirports(double latitude, double longitude)
        {
             Airports = await _airportsService.GetAllAirports(latitude, longitude);
        }
    }
}
