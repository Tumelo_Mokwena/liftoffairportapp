﻿namespace LiftOffAirportApp.ViewModels
{
    using MvvmCross.ViewModels;
    using System.Threading.Tasks;

    public class BaseViewModel : MvxViewModel
    {
        public BaseViewModel()
        { }

        public override Task Initialize()
        {
            return base.Initialize();
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
        private string _title;
    }
}
