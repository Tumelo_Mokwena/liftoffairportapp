﻿namespace LiftOffAirportApp.Models.Models
{
    public class AirportModel
    {
        public string airportId { get; set; }
        public string nameAirport { get; set; }
        public string codeIataAirport { get; set; }
        public string codeIcaoAirport { get; set; }
        public string latitudeAirport { get; set; }
        public string longitudeAirport { get; set; }
        public string geonameId { get; set; }
        public string timezone { get; set; }
        public string GMT { get; set; }
        public string phone { get; set; }
        public string nameCountry { get; set; }
        public string codeIso2Country { get; set; }
        public string codeIataCity { get; set; }
    }

}
