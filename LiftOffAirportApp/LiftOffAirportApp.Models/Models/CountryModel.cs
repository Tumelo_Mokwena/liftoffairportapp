﻿namespace LiftOffAirportApp.Models.Models
{
    public class CountryModel
    {
        public string countryId { get; set; }
        public string nameCountry { get; set; }
        public string codeIso2Country { get; set; }
        public string codeIso3Country { get; set; }
        public string numericIso { get; set; }
        public string population { get; set; }
        public string capital { get; set; }
        public string continent { get; set; }
        public string nameCurrency { get; set; }
        public string codeCurrency { get; set; }
        public string codeFips { get; set; }
        public string phonePrefix { get; set; }
    }
}
