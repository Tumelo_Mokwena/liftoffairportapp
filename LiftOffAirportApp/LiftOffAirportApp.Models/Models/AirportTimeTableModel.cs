﻿namespace LiftOffAirportApp.Models.Models
{
    using System;

    public class AirportTimeTableModel
    {

        public string type { get; set; }
        public string status { get; set; }
        public Departure departure { get; set; }
        public Arrival arrival { get; set; }
        public Airline airline { get; set; }
        public Flight flight { get; set; }
        public Codeshared codeshared { get; set; }
    }

    public class Departure
    {
        public string iataCode { get; set; }
        public string icaoCode { get; set; }
        public DateTime scheduledTime { get; set; }
        public DateTime estimatedTime { get; set; }
        public DateTime actualTime { get; set; }
        public DateTime estimatedRunway { get; set; }
        public DateTime actualRunway { get; set; }
    }

    public class Arrival
    {
        public string iataCode { get; set; }
        public string icaoCode { get; set; }
        public string terminal { get; set; }
        public string baggage { get; set; }
        public DateTime scheduledTime { get; set; }
        public DateTime estimatedTime { get; set; }
        public DateTime actualTime { get; set; }
    }

    public class Airline
    {
        public string name { get; set; }
        public string iataCode { get; set; }
        public string icaoCode { get; set; }
    }

    public class Flight
    {
        public string number { get; set; }
        public string iataNumber { get; set; }
        public string icaoNumber { get; set; }
    }

    public class Codeshared
    {
        public Airline airline { get; set; }
        public Flight flight { get; set; }
    }
}
