﻿namespace LiftOffAirportApp.Models.Models
{
    using System;

    public class AirplaneModel
    {
        public string airplaneId { get; set; }
        public string numberRegistration { get; set; }
        public string productionLine { get; set; }
        public string airplaneIataType { get; set; }
        public string planeModel { get; set; }
        public string modelCode { get; set; }
        public string hexIcaoAirplane { get; set; }
        public string codeIataPlaneShort { get; set; }
        public string codeIataPlaneLong { get; set; }
        public string constructionNumber { get; set; }
        public string numberTestRgistration { get; set; }
        public string rolloutDate { get; set; }
        public DateTime firstFlight { get; set; }
        public DateTime deliveryDate { get; set; }
        public DateTime registrationDate { get; set; }
        public string lineNumber { get; set; }
        public string planeSeries { get; set; }
        public string codeIataAirline { get; set; }
        public string codeIcaoAirline { get; set; }
        public string planeOwner { get; set; }
        public string enginesCount { get; set; }
        public string enginesType { get; set; }
        public string planeAge { get; set; }
        public string planeStatus { get; set; }
    }
}
