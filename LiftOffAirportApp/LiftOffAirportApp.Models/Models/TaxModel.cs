﻿namespace LiftOffAirportApp.Models.Models
{
    public class TaxModel
    {
        public string taxId { get; set; }
        public string nameTax { get; set; }
        public string codeIataTax { get; set; }
    }
}

