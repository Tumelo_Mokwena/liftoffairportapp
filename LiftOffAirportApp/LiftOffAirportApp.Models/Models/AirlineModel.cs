﻿namespace LiftOffAirportApp.Models.Models
{
    public class AirlineModel
    {
        public string airlineId { get; set; }
        public string nameAirline { get; set; }
        public string codeIataAirline { get; set; }
        public string iataPrefixAccounting { get; set; }
        public string codeIcaoAirline { get; set; }
        public string callsign { get; set; }
        public string type { get; set; }
        public string statusAirline { get; set; }
        public string sizeAirline { get; set; }
        public string ageFleet { get; set; }
        public string founding { get; set; }
        public string codeHub { get; set; }
        public string nameCountry { get; set; }
        public string codeIso2Country { get; set; }
    }
}
