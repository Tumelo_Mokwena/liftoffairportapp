﻿namespace LiftOffAirportApp.Models.Models
{
    public class AircraftTypeModel
    {
        public string planeTypeId { get; set; }
        public string nameAircraft { get; set; }
        public string codeIataAircraft { get; set; }
    }
}
