﻿namespace LiftOffAirportApp.Views.AirportsMap
{
    using Android.App;
    using Android.Content.PM;
    using Android.Locations;
    using Android.OS;
    using Android.Runtime;
    using Android.Support.Design.Widget;
    using Android.Support.V4.App;
    using Android.Support.V4.Content;
    using Android.Views;
    using Android.Widget;
    using LiftOffAirportApp.Services.Services.Interfaces;
    using MvvmCross.Navigation;
    using System;
    using Android.Gms.Maps;
    using ViewModels.AirportsMap;
    using Android.Gms.Maps.Model;
    using MvvmCross.Droid.Support.V7.AppCompat;

    [Activity(Label = "Airports around", MainLauncher = true, LaunchMode = LaunchMode.SingleTask)]
    public class AirportsMapActivity : MvxAppCompatActivity<AirportsViewModel>, ILocationListener, IOnMapReadyCallback
    {
        View mainLayout;

        Button permissionButton;
        const long ONE_MINUTE = 60 * 1000;
        const long FIVE_MINUTES = 5 * ONE_MINUTE;

        static readonly int LOCATION_PERMISSION_CHECK = 1000;
        static readonly int LOCATION_UPDATES_PERMISSION_CHECK = 1100;

        IMvxNavigationService navigationService;
        IAirportsService airportsService;

        LocationManager locationManager;
        GoogleMap googleMap;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(LiftOffAirportApp.Resource.Layout.activity_airports_map);

            locationManager = (LocationManager)GetSystemService(LocationService);

            mainLayout = FindViewById(LiftOffAirportApp.Resource.Id.airportsMap_layout);

            permissionButton = FindViewById<Button>(LiftOffAirportApp.Resource.Id.permissionButton);
            InitializeMap();

            if (locationManager.AllProviders.Contains(LocationManager.NetworkProvider)
                && locationManager.IsProviderEnabled(LocationManager.NetworkProvider))
            {
                permissionButton.Click += LocationPermissionButtonOnClick;
            }
            else
            {
                Snackbar.Make(mainLayout, LiftOffAirportApp.Resource.String.permission_not_granted, Snackbar.LengthIndefinite)
                        .SetAction(LiftOffAirportApp.Resource.String.okText, delegate { FinishAndRemoveTask(); })
                        .Show();
            }
        }

        private void InitializeMap()
        {
            FragmentManager.FindFragmentById<MapFragment>(LiftOffAirportApp.Resource.Id.mapFragment).GetMapAsync(this);
        }

        async void LocationPermissionButtonOnClick(object sender, EventArgs eventArgs )
        {
            if (ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.AccessFineLocation) == Permission.Granted)
            {
                var criteria = new Criteria { PowerRequirement = Power.Medium };

                var bestProvider = locationManager.GetBestProvider(criteria, true);
                var location = locationManager.GetLastKnownLocation(bestProvider);

                if (location != null)
                {
                    permissionButton.Visibility = ViewStates.Invisible;

                    await ViewModel.GetAirports(location.Latitude, location.Longitude);
                    var currentLocation = new LatLng(Convert.ToDouble(location.Latitude), Convert.ToDouble(location.Longitude));

                    var cameraUpdate = CameraUpdateFactory.NewLatLngZoom(currentLocation, 12);
                    googleMap.MoveCamera(cameraUpdate);
                }
            }
            else
            {
                if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Android.Manifest.Permission.AccessFineLocation))
                {
                    Snackbar.Make(mainLayout, LiftOffAirportApp.Resource.String.permission_not_granted, Snackbar.LengthIndefinite)
                            .SetAction(LiftOffAirportApp.Resource.String.okText,
                                       delegate
                                       {
                                           ActivityCompat.RequestPermissions(this, new[] { Android.Manifest.Permission.AccessFineLocation }, LOCATION_PERMISSION_CHECK);
                                       })
                            .Show();
                }
                else
                {
                    ActivityCompat.RequestPermissions(this, new[] { Android.Manifest.Permission.AccessFineLocation }, LOCATION_PERMISSION_CHECK);
                }
            }
        }

        public void OnMapReady(GoogleMap gMap)
        {
            googleMap = gMap;

            googleMap.UiSettings.ZoomControlsEnabled = true;
            googleMap.UiSettings.CompassEnabled = true;
            googleMap.UiSettings.MyLocationButtonEnabled = false;
            AddMarkers();
        }

        void InitialiseOnMapSettings()
        {
            googleMap.UiSettings.MyLocationButtonEnabled = true;
            googleMap.UiSettings.CompassEnabled = true;
            googleMap.UiSettings.ZoomControlsEnabled = true;
            googleMap.MyLocationEnabled = true;

        }

        void AddMarkers()
        {
            AirportsViewModel viewModel = new AirportsViewModel(navigationService, airportsService);

            if (viewModel.Airports != null)
            {
                foreach (var item in viewModel.Airports)
                {
                    var airportLatLng = new LatLng(Convert.ToDouble(item.latitudeAirport), Convert.ToDouble(item.longitudeAirport));

                    var marker = new MarkerOptions();
                    marker.SetPosition(airportLatLng)
                                .SetTitle(item.nameAirport)
                                .SetIcon(BitmapDescriptorFactory.DefaultMarker(BitmapDescriptorFactory.HueCyan));
                    googleMap.AddMarker(marker);
                }
            }
        }

        public void OnLocationChanged(Location location)
        {
            throw new System.NotImplementedException();
        }

        public void OnProviderDisabled(string provider)
        {
            throw new System.NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new System.NotImplementedException();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            throw new System.NotImplementedException();
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}