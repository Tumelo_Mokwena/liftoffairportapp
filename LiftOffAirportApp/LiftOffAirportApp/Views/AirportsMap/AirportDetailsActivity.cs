﻿namespace LiftOffAirportApp.Views.AirportsMap
{
    using Android.App;
    using Android.OS;
    using LiftOffAirportApp.ViewModels.ViewModels.AirportsMap;
    using MvvmCross.Droid.Support.V7.AppCompat;

    [Activity(Label = "AirportDetailsActivity")]
    public class AirportDetailsActivity : MvxAppCompatActivity<AirportDetailsViewModel>
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //SetContentView(Resource.Layout.activity_airport_details);
        }
    }
}