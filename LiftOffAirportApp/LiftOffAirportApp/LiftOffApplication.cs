﻿namespace LiftOffAirportApp.Droid
{
    using Android.App;
    using ViewModels;
    using Android.Runtime;
    using MvvmCross.Droid.Support.V7.AppCompat;
    using System;

    [Application]
    public class LiftOffApplication : MvxAppCompatApplication<MvxAppCompatSetup<App>, App>
    {
        public LiftOffApplication(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }
    }
}