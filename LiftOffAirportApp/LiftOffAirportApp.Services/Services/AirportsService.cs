﻿namespace LiftOffAirportApp.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using LiftOffAirportApp.Models.Models;
    using LiftOffAirportApp.Services.Helpers;
    using LiftOffAirportApp.Services.Services.Interfaces;
    using Newtonsoft.Json;
    using NLog;
    using RestSharp;

    public class AirportsService : IAirportsService
    {
        private HttpHelper _httpHelper;
        private AirportModel _airport;
        private IEnumerable<AirportModel> _airports;
        private Logger _logger;

        public AirportsService()
        {
            _httpHelper = new HttpHelper();
            _logger = LogManager.GetLogger("fileLogger");
        }

        public AirportModel GetAirportDetails(string airportId)
        {
            try
            {
                var client = _httpHelper.InitializeAirportDetails(airportId);

                IRestResponse response = client.Execute(_httpHelper.ClientRequest);

                if (response.IsSuccessful && response.Content != null)
                {
                    _airport = JsonConvert.DeserializeObject<AirportModel>(response.Content);
                }
            }
            catch(Exception ex)
            {
                _logger.Error(ex.Message);
            }
            return _airport;
        }

        public async Task<IEnumerable<AirportModel>> GetAllAirports(double longitude, double latitude)
        {
            try
            {
                var client = _httpHelper.InitializeNearbyAirports(longitude, latitude);

                IRestResponse response = client.Execute(_httpHelper.ClientRequest);

                if (response.IsSuccessful && response.Content != null)
                {
                    _airports = JsonConvert.DeserializeObject<IEnumerable<AirportModel>>(response.Content);
                }
            }
            catch(Exception ex)
            {
                _logger.Error(ex.Message);
            }
            
            return _airports;
        }
    }
}
