﻿namespace LiftOffAirportApp.Services.Services.Interfaces
{
    using LiftOffAirportApp.Models.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAirportsService
    {
        Task<IEnumerable<AirportModel>> GetAllAirports(double longitude, double latitude);
    }
}
