﻿namespace LiftOffAirportApp.Services.Services.Interfaces
{
    using LiftOffAirportApp.Models.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAirportTimeTableService
    {
        Task<IEnumerable<AirportTimeTableModel>> GetAirportTimeTableById(string airportId);
    }
}
