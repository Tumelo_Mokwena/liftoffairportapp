﻿namespace LiftOffAirportApp.Services.Services
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using LiftOffAirportApp.Models.Models;
    using LiftOffAirportApp.Services.Helpers;
    using LiftOffAirportApp.Services.Services.Interfaces;
    using NLog;
    using RestSharp;

    public class AirportTimeTableService : IAirportTimeTableService
    {
        private HttpHelper _httpHelper;
        private Task<IEnumerable<AirportTimeTableModel>> _airportTimeTable;
        private Logger _logger;

        public AirportTimeTableService()
        {
            _httpHelper = new HttpHelper();
            _logger = LogManager.GetLogger("fileLogger");
        }

        public async Task<IEnumerable<AirportTimeTableModel>> GetAirportTimeTableById(string airportId)
        {
            try
            {
                var client = _httpHelper.InitializeAirportDetails(airportId);

                IRestResponse response = client.Execute(_httpHelper.ClientRequest);

                if (response.IsSuccessful && response.Content != null)
                {
                   // TODO: Implement airtport time tables once nearby airports service is done
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
            }
            return await _airportTimeTable;
        }
    }
}
