﻿namespace LiftOffAirportApp.Services.Helpers
{
    using RestSharp;

    public class HttpHelper
    {
        public RestClient Client;
        public RestRequest ClientRequest;

        public HttpHelper()
        { }

        public RestClient InitializeNearbyAirports(double longitude, double latitude)
        {
            var url = ServiceConstants.BASE_URL + "public/nearby?";
            string api_key = "719cd8-395517";
            Client = new RestClient(url);
            ClientRequest = new RestRequest(Method.GET);
            ClientRequest.AddHeader("Content-Type", "application/json;charset=utf-8");
            ClientRequest.AddJsonBody(new { key = api_key, lat = latitude, lng = longitude, distance = 1});
            return Client;
        }

        public RestClient InitializeAirportDetails(string airportId)
        {
            var url = ServiceConstants.BASE_URL + "public/timetable?";
            string api_key = "719cd8-395517";
            Client = new RestClient(url);
            ClientRequest = new RestRequest(Method.GET);
            ClientRequest.AddHeader("Content-Type", "application/json;charset=utf-8");
            ClientRequest.AddJsonBody(new { key = api_key, iataCode = airportId, type = "departure"});
            return Client;
        }
    }
}
